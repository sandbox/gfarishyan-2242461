<?php
/**
 * Implements hook_views_plugins().
 */
function views_threaded_comments_views_plugins() {
    return array(
	     'module' => 'views_threaded_comments',
	     'style' => array(
	       'threaded_comments' => array(
	         'title' => t('Threaded comments'),
	         'help' => t('Displays comments as threads.'),
	         'handler' => 'views_plugin_style_threaded_comments',
	         'path' => drupal_get_path('module', 'views_threaded_comments'),
	         'theme' => 'views_threaded_comments',
	         'uses row plugin' => TRUE,
	         'uses row class' => TRUE,
	         //'uses fields' => TRUE,
	         'uses options' => TRUE,
	         'uses grouping' => FALSE,
	         'type' => 'normal',
	         'help topic' => 'style-threaded_comments',
	         'base' => array('comment'), // only works with 'comment' as base.
	       ),
	     ),
  );
}

/**
 * Display the view as threaded list of comments
 */
function template_preprocess_views_threaded_comments(&$vars) {
  template_preprocess_views_view_unformatted($vars);
}