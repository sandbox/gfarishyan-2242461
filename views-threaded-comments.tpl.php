<?php
// $Id$
/**
 * @file views-view-comment-thread.tpl.php
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php
  $divs = 0;
  $comments = '';
  drupal_add_css(drupal_get_path('module', 'comment') .'/comment.css');
  foreach ($rows as $id => $row) {
    $comment = $view->style_plugin->row_plugin->comments[$view->result[$id]->cid];
    if ($comment->depth > $divs) {
      $divs++;
      $comments .= '<div class="indented">';
    }
    else {
      while ($comment->depth < $divs) {
        $divs--;
        $comments .= '</div>';
      }
    }
    $comments .= "<div class=" . $classes[$id] . ">" . $row . "</div>";
  }
  while ($divs-- > 0) {
    $comments .= '</div>';
  }
print $comments;
